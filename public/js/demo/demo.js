/*
 * blueimp Gallery Demo JS
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global blueimp, $ */

$(function () {
  'use strict'
  var photos = 
  [
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993936585.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993962154.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484994012652.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993939322.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484994057889.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/P60621-141900.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993909328.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/P60621-141827.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484995097404.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484995071995.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484994675976.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1479693885324.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993817503.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993821837.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993834895.jpg"
    },
    {
        "href": "http://ouzmpyi08.bkt.clouddn.com/mmexport1484993997723.jpg"
    }
];
  var carouselLinks = []
  var linksContainer = $('#links')
  var baseUrl
  var title
  $.each(photos, function (index, photo) {
      baseUrl = photo.href;
      title = "my girl";
      $('<a/>')
        .append($('<img>').prop('src', baseUrl+"?imageView2/1/w/150/h/150"))
        .prop('href', baseUrl)
        .prop('title', title)
        .attr('data-gallery', '')
        .appendTo(linksContainer)
      carouselLinks.push({
        href: baseUrl ,
        title: title
      })
    })
    // Initialize the Gallery as image carousel:
    blueimp.Gallery(carouselLinks, {
      container: '#blueimp-image-carousel',
      carousel: true
    })
})
